<?php
/**
 * GA Meta Tags is the very helpful and very simple plugin for WordPress
 * that adds all important Meta-Tags Site's Header Section automatically
 * without changing any of your themes file and without slowing down your site.
 *
 *
 * @package GA Meta Tags
 * @author Gevorg Andreasyan
 * @license GPL-2.0+
 * @link http://andreasyan.net/
 * @copyright 2016 Gevorg Andreasyan. All rights reserved.
 *
 *            @wordpress-plugin
 *            Plugin Name: GA Meta Tags
 *            Plugin URI: http://andreasyan.net/
 *            Description: GA Meta Tags is the very helpful and very simple plugin for WordPress that adds all important Meta-Tags Site's Header Section automatically without changing any of your themes file and without slowing down your site.
 *            Version: 1.0
 *            Author: Gevorg Andreasyan
 *            Author URI: http://andreasyan.net/
 *            Text Domain: ga-meta-tags
 *            Contributors: Gevorg Andreasyan
 *            License: GPL-2.0+
 *            License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

/*
 * Copyright (C) 2016 Andreasyan.net This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

require_once (dirname ( __FILE__ ) . '/pages/ga-meta-tags-functions.php');