=== GA Meta Tags === 
Contributors: Gevorg Andreasyan
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=HTNUNR3NSYSNG
Tags: GA Meta Tags, Gevorg Andreasyan, Meta Tags Plugin, Meta Plugin, Tags Plugin, SEO, Search Engine Optimization, GA Portfolio Premium Plugin, SEO Plugin, SEO Free Plugin, SEO Free, Meta Tags Free
Requires at least: 4.4.2
Tested up to: 4.5.2
Stable tag: 1.0
License: GPLv2

Very helpful and simple plguin for WordPress SEO.

== Description ==

If you already know about the word SEO then you probably know about such meta tags.

Subscribe to <a href="https://twitter.com/ga_portfolio">Twitter</a> and <a href="https://www.facebook.com/gaportfolio">Facebook</a> to get **latest update**

**Meta Options**

*Meta Description
*Meta Keywords
*Meta Robots
*Meta Revisit
*Meta Generator
*Meta Author
*Meta Contact
*Meta Copyright
*Meta Language

== Installation ==
1. Unpack the `download-package`.
2. Upload the file to the `/wp-content/plugins/` directory.
3. Activate the plugin through the `Plugins` menu in WordPress.
4. Configure the options under Admin Panel `GA Meta Tags -> List of all Options`.
5. Done and Ready.

== Frequently Asked Questions ==

= Got a Question? =
* Please report your questions or bugs at Plugin <a href="http://andreasyan.net/portfolio-archive/ga-meta-tags/" target="_blank">Homepage</a>.

== Screenshots ==
1. images/upload ga-meta plugin
2. images/go to plugins
3. images/activate plugin
4. images/edit and save options
5. images/check results

== Changelog ==

= 1.0 =

* Compatibility with WordPress 4.5.2


