<?php
/**
 * @author Andreasyan.net
 * Plugin: GA Meta Tags
 * URL: http://andreasyan.net/
 */
?>
<?php

    /**
     * Register All Menus.
     *
     * @since 1.0
     */
    add_action ( 'admin_menu', 'ga_meta_tags_menu' );

    function ga_meta_tags_menu(){
        add_menu_page ( 'GA Meta Tags', 'GA Meta Tags', 'manage_options', 'ga-meta-tags', 'ga_meta_tags_home_page', plugins_url ( 'ga-meta/images/favicon.ico' ), 6 );
        add_submenu_page ( 'ga-meta-tags', 'Main Options', 'Main Options', 'manage_options', 'ga-meta-tags', 'ga_meta_tags_home_page' );        
    }

    /**
     * Load CSS and JS.
     *
     * @since 1.0
     */
    add_action ( 'admin_menu', 'ga_meta_tags_load_css_js' );

    function ga_meta_tags_load_css_js() {
        wp_enqueue_script ( 'jquery' ); // Enque Default jQuery
        wp_enqueue_script ( 'jquery-ui-core' ); // Enque Default jQuery UI Core
        wp_enqueue_script ( 'jquery-ui-tabs' ); // Enque Default jQuery UI Tabs
    
        wp_register_script ( 'ga-meta-tags-plugin-bootstrap-script', plugins_url ( '../js/bootstrap.min.js', __FILE__ ) );
        wp_enqueue_script ( 'ga-meta-tags-plugin-bootstrap-script' );
        wp_register_script ( 'ga-meta-tags-plugin-script', plugins_url ( '../js/ga-meta-tags.js', __FILE__ ) );
        wp_enqueue_script ( 'ga-meta-tags-plugin-script' );
    
        wp_register_style ( 'ga-meta-tags-plugin-bootstrap', plugins_url ( '../css/bootstrap.min.css', __FILE__ ) );
        wp_enqueue_style ( 'ga-meta-tags-plugin-bootstrap' );
        wp_register_style ( 'ga-meta-tags-plugin-bootstrap-theme', plugins_url ( '../css/bootstrap-theme.min.css', __FILE__ ) );
        wp_enqueue_style ( 'ga-meta-tags-plugin-bootstrap-theme' );
        wp_register_style ( 'ga-meta-tags-plugin-css', plugins_url ( '../css/ga-meta-tags.css', __FILE__ ) );
        wp_enqueue_style ( 'ga-meta-tags-plugin-css' );

    }

    /**
     * Get all Header Meta Tags
     *
     * @since 1.0
     */
    add_action ( 'wp_head', 'ga_meta_tags_header', 2 );
    
    function ga_meta_tags_header(){
        $meta_tags_description = get_option ( 'ga_meta_tags_description' );
        $meta_tags_keywords = get_option ( 'ga_meta_tags_keyword' );
        $meta_tags_robots = get_option ( 'ga_meta_tags_robots' );
        $meta_tags_revisit = get_option ( 'ga_meta_tags_revisit' );
        $meta_tags_generator = get_option ( 'ga_meta_tags_generator' );
        $meta_tags_author = get_option ( 'ga_meta_tags_author' );
        $meta_tags_contact = get_option ( 'ga_meta_tags_contact' );
        $meta_tags_copyright = get_option ( 'ga_meta_tags_copyright' );
        $meta_tags_language = get_option ( 'ga_meta_tags_language' );

        echo "\n";
        echo "<!-- GA Meta Tags plugin by Andreasyan.net -->\n";

        if (! ($meta_tags_description == "")) {
            $meta_tags_description_meta = '<meta name="description" content="' . sanitize_text_field($meta_tags_description) . '" /> ';
            echo $meta_tags_description_meta . "\n";
        }
        if (! ($meta_tags_keywords == "")) {
            $meta_tags_keywords_meta = '<meta name="keywords" content="' . sanitize_text_field($meta_tags_keywords) . '" /> ';
            echo $meta_tags_keywords_meta. "\n";
        }
        if (! ($meta_tags_robots == "")) {
            $meta_tags_robots_meta = '<meta name="robots" content="' . sanitize_text_field($meta_tags_robots) . '" /> ';
            echo $meta_tags_robots_meta . "\n";
        }
        if (! ($meta_tags_revisit == "")) {
            $meta_tags_revisit_meta = '<meta name="revisit-after" content="' . sanitize_text_field($meta_tags_revisit) . '" /> ';
            echo $meta_tags_revisit_meta . "\n";
        }
        if (! ($meta_tags_generator == "")) {
            $meta_tags_generator_meta = '<meta name="generator" content="' . sanitize_text_field($meta_tags_generator) . '" /> ';
            echo $meta_tags_generator_meta . "\n";
        }
        if (! ($meta_tags_author == "")) {
            $meta_tags_author_meta = '<meta name="author" content="' . sanitize_text_field($meta_tags_author) . '" /> ';
            echo $meta_tags_author_meta . "\n";
        }
        if (! ($meta_tags_contact == "")) {
            $meta_tags_contact_meta = '<meta name="contact" content="' . sanitize_text_field($meta_tags_contact) . '" /> ';
            echo $meta_tags_contact_meta . "\n";
        }
        if (! ($meta_tags_copyright == "")) {
            $meta_tags_copyright_meta = '<meta name="copyright" content="' . sanitize_text_field($meta_tags_copyright) . '" /> ';
            echo $meta_tags_copyright_meta . "\n";
        }
        if (! ($meta_tags_language == "")) {
            $meta_tags_language_meta = '<meta name="language" content="' . sanitize_text_field($meta_tags_language) . '" /> ';
            echo $meta_tags_language_meta . "\n";
        }
        
        echo "<!-- /GA Meta Tags plugin -->\n\n";
    }

    /**
     * Get all Footer Meta Tags
     *
     * @since 1.0
     */
    add_action ( 'wp_footer', 'ga_meta_tags_footer', 1000 );
    
    function ga_meta_tags_footer(){
        
    }

    /**
     * Initialize Variable.
     *
     * @since 1.0
     */
    add_option ( 'ga_meta_tags_description', '' );
    add_option ( 'ga_meta_tags_keyword', '' );
    add_option ( 'ga_meta_tags_robots', '' );
    add_option ( 'ga_meta_tags_revisit', '' );
    add_option ( 'ga_meta_tags_generator', '' );
    add_option ( 'ga_meta_tags_author', '' );
    add_option ( 'ga_meta_tags_contact', '' );
    add_option ( 'ga_meta_tags_copyright', '' );
    add_option ( 'ga_meta_tags_language', '' );

    /**
     * Save All Options.
     *
     * @since 1.0
     */
    function ga_meta_tags_save_all_options() {

        if (isset ( $_POST ['update_home'] )) {
            if( current_user_can('administrator') ){
                if (! isset ( $_POST ['ga_meta_tags_update_setting'] )){
                    die ( "Looks like you didn't send any credentials." );
                }

                update_option ( 'ga_meta_tags_description', ( string ) $_POST ["ga_meta_tags_description"] );
                update_option ( 'ga_meta_tags_keyword', ( string ) $_POST ["ga_meta_tags_keyword"] );
                update_option ( 'ga_meta_tags_robots', ( string ) $_POST ["ga_meta_tags_robots"] );
                update_option ( 'ga_meta_tags_revisit', ( string ) $_POST ["ga_meta_tags_revisit"] );
                update_option ( 'ga_meta_tags_generator', ( string ) $_POST ["ga_meta_tags_generator"] );
                update_option ( 'ga_meta_tags_author', ( string ) $_POST ["ga_meta_tags_author"] );
                update_option ( 'ga_meta_tags_contact', ( string ) $_POST ["ga_meta_tags_contact"] );
                update_option ( 'ga_meta_tags_copyright', ( string ) $_POST ["ga_meta_tags_copyright"] );
                update_option ( 'ga_meta_tags_language', ( string ) $_POST ["ga_meta_tags_language"] );

                echo '<div id="message" class="updated fade"><p><strong>Main Settings Updated.</strong></p></div>';
                echo '</strong>';
            }
            else{
                die ( "The fields can change only administrator." );
            }
        }
    }

    /**
     * Add Home Page.
     *
     * @since 1.0
     */
    function ga_meta_tags_home_page() {
        ga_meta_tags_save_all_options ();
        require_once (dirname ( __FILE__ ) . '/ga-meta-tags-home.php');
    }
