<?php
/**
 * @author Gevorg Andreasyan
 * Plugin: GA Meta Tags
 * URL: http://andreasyan.net/
 */
?>

<div class="container-fluid">
    <form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
        <input type="hidden" name="info_update1" id="info_update1" value="true" />
        <h1>GA Meta Tags Options</h1>
        <div class="left">
            <br>
            <a href="https://twitter.com/ga_portfolio" class="twitter-follow-button" data-show-count="false">
                Follow @GA_Portfolio
            </a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>

            <div class="fb-like" data-href="https://www.facebook.com/gaportfolio"
                 data-layout="standard" data-action="like" data-show-faces="false"
                 data-share="true"></div>
        </div>
        <div id="poststuff" class="metabox-holder has-right-sidebar container-fuild">
            <div class="col-md-9">
                <div class="row">
