<?php
/**
 * @author Gevorg Andreasyan
 * Plugin: GA Meta Tags
 * URL: http://andreasyan.net/
 */
?>



<div class="clear">
    <p>
        <br />&copy; <?php echo date("Y"); ?> <a
            href="http://andreasyan.net" target="_blank">Andreasyan.net</a>
    </p>
</div>
