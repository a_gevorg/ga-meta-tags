<?php
/**
 * @author Gevorg Andreasyan
 * Plugin: GA Meta Tags
 */
?>

<div class="col-md-3">
    <div id="side-info-column" class="inner-sidebar">
        <div class="postbox">
            <span class="rate-us">
                <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=HTNUNR3NSYSNG" target="_blank">				   
				   <h3>If You like our plugin You can donate some money for our work</h3>
				</a>
            </span>
        </div>
    </div>
    <div id="side-info-column" class="inner-sidebar">
        <div class="postbox">
            <h2>
                List of All Features
            </h2>
            <hr/>
            <div class="inside">
                <ul>
                    <li><h3>Meta Description</h3></li>
                    <li><h3>Meta Keywords</h3></li>
                    <li><h3>Meta Robots</h3></li>
                    <li><h3>Meta Revisit</h3></li>
                    <li><h3>Meta Generator</h3></li>
                    <li><h3>Meta Author</h3></li>
                    <li><h3>Meta Contact</h3></li>
                    <li><h3>Meta Copyright</h3></li>
                    <li><h3>Meta Language</h3></li>
                </ul>
            </div>
        </div>
    </div>
</div>